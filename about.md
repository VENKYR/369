---
layout: page
title: About
permalink: /about/
---

This is personal blog developed using [jekyllrb.com](http://jekyllrb.com/)

You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)
